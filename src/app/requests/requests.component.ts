import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PostService } from '../shared/post.service';
import { UserService } from '../shared/user.service';

@Component({
  selector: 'app-requests',
  templateUrl: './requests.component.html',
  styleUrls: ['./requests.component.css']
})
export class RequestsComponent implements OnInit {
user ;
requests = [];
  constructor(private userService: UserService, private router: Router,
    private postService:PostService,
    private _activatedRoute: ActivatedRoute,) { }

  ngOnInit(): void {
   this.getUser();
  }
  approve(event){
    this.userService.approveRequest({
      _id:this.user._id,
      name:this.user.name,
      user_id:event.userId,
      user_name:event.username

    }).subscribe(
      res => {
        this.getUser()
        alert("Now you and " + event.username + " are friends")
      },
      err => {
        console.log(err);
    
      })
  }
  reject(event){
    console.log("event",event)
    this.userService.rejectRequest({
      _id:this.user._id,
      name:this.user.name,
      user_id:event.userId,
      user_name:event.username

    }).subscribe(
      res => {
       this.getUser()
       alert("Friend request rejected!!")

      },
      err => {
        console.log(err);
    
      })
  }
  getUser(){
    this.userService.getUserProfile().subscribe(
      res => {
        this.user= res['user'];
        console.log("user",this.user)
        this.requests = this.user['request'];
        console.log("req",this.requests)
      },
      err => {
        console.log(err);
    
      })
    
  }
  

}
