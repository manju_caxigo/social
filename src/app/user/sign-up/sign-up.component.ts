import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';
import { Router } from "@angular/router";

import { UserService } from '../../shared/user.service'

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css'],
  providers: [UserService]
})
export class SignUpComponent implements OnInit {
  emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  constructor(public userService: UserService,private router: Router) { }

  ngOnInit() {
  }
  showSucessMessage: boolean;
serverErrorMessages: string;

onSubmit(form: NgForm) {
    this.userService.postUser(form.value).subscribe(
      res => {
        console.log("res",res)
        this.showSucessMessage = true;
        setTimeout(() => this.showSucessMessage = false, 4000);
        this.router.navigate(['/login']);
        this.resetForm(form);
      },
      err => {
        console.log("err",err)
        if (err.status === 422) {
          this.serverErrorMessages = err.error;
        }
        else
          this.serverErrorMessages = 'Something went wrong.';
      }
    );
  }

 resetForm(form: NgForm) {
    this.userService.selectedUser = {
      name: '',
      age:0,
      email: '',
      password: ''
    };
    form.resetForm();
    this.serverErrorMessages = '';
  }


}