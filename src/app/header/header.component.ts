import { Component, OnInit, Input,
  Output,
  EventEmitter, } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';

import { UserService } from '../shared/user.service';
import { trigger } from '@angular/animations';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  userDetails;
  user
  search;
  nouser: boolean =false;

  constructor(private userService: UserService, private router: Router,
    private formBuilder: FormBuilder,) { }

  ngOnInit() {
  
  this.user = this.userService.isLoggedIn()
  console.log("user",this.userDetails)
  this.userService.getUserProfile().subscribe(
  res => {
    this.userDetails = res['user'];
    console.log("details",this.userDetails)
  },
  err => {
    console.log(err);
  }
);
  }
  getFriendReq(){
    this.router.navigate(['/requests']);
  }
  onLogout(){
    this.userService.deleteToken();
    this.router.navigate(['/login']);
  }
  gotoProfile(){
    this.router.navigate(['/userprofile']);
  }
  goHome(){
    this.router.navigate(['/home']);
  }
  searchUser(){
    console.log(this.search)
    
    this.userService.getUserByName(this.search).subscribe(
      res => {
        this.userDetails = res['user'];
        console.log("name",this.userDetails)
        if(this.userDetails===undefined){
          alert("No user found.")
          this.search ='';
        }
        else
        this.router.navigate(['/userprofile/'+this.userDetails._id]);

      },
      err => {
   
        console.log(err.error.message);
     
      }
    );
  }

}
