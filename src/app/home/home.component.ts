

import { Component, ViewChild, ElementRef,OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import * as _ from 'lodash';
import { Router } from '@angular/router';
import { UserService } from '../shared/user.service';
import {PostService } from '../shared/post.service';

@Component({
  selector: 'app-home',
  templateUrl:'./home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  @ViewChild('UploadFileInput', { static: false }) uploadFileInput: ElementRef;
  fileUploadForm: FormGroup;
  fileInputLabel: string;
  imageUrl:String;
  data = '';
  show = false;
  create = false;
  userDetails;
  posts = []

  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder,
    private userService: UserService, private router: Router,
    private postService:PostService
  ) { }

  ngOnInit(): void {
    this.fileUploadForm = this.formBuilder.group({
      uploadedImage: [''],
      data:['']
    });
    this.userService.getUserProfile().subscribe(
      res => {
        this.userDetails = res['user'];
        console.log("details",this.userDetails)
      },
      err => {
        console.log(err);
      }
    );
  this.getAllPosts()
  }

  
getAllPosts(){
  this.postService.getAllPosts().subscribe(
    (res: Array<any>)=> {       
      console.log("posts",res);
      this.posts = res
    },
    err => {
      console.log(err);
    }
  );

}
uploadPhoto(){
  this.create =true;
}
cancelUpload(){
  this.create =false;
}
  onFileSelect(event) {
    if (event.target.files && event.target.files[0]) {
        const file = event.target.files[0];
        this.fileInputLabel = file.name;
        this.fileUploadForm.get('uploadedImage').setValue(file)
        this.show = true;
        const reader = new FileReader();
        reader.onload = e => this.imageUrl = reader.result as string;
        reader.readAsDataURL(file);
    }
}

  savePost(){
    console.log("pst",this.data,this.imageUrl,this.userDetails._id)
  
  }
  onFormSubmit() {

    if (!this.fileUploadForm.get('uploadedImage').value) {
      alert('Select a photo!');
      return false;
    }
    if(!this.data){
      alert('Add a description!');
      return false;
    }

    const formData = new FormData();
    formData.append('uploadedImage', this.fileUploadForm.get('uploadedImage').value);
    formData.append('agentId', '007');

    console.log("formdata",formData)
    this.http
      .post<any>(environment.apiBaseUrl+'/uploadfile', formData).subscribe(response => {
        console.log("imageupp",response);
        if (response.statusCode === 200) {
          this.imageUrl =  response.uploadedFile.filename;
          this.postService.savePost({
            data:this.data,
            image:this.imageUrl,
            user_id:this.userDetails._id
          }).subscribe(res=>{
            console.log("response",res)
            // Reset the file input
            console.log("image",this.imageUrl)
            this.uploadFileInput.nativeElement.value = "";
            this.fileInputLabel = undefined;
            this.show = false;
            this.getAllPosts()

          }, er => {
            console.log(er);
            alert(er.error.error);
          })          
        }
      }, er => {
        console.log(er);
        alert(er.error.error);
      });
  }

  onLogout(){
    this.userService.deleteToken();
    this.router.navigate(['/login']);
  }
}
