import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  constructor(private http: HttpClient) { }

  noAuthHeader = { headers: new HttpHeaders({ 'NoAuth': 'True' }) };

  savePost(data){
      return this.http.post(environment.apiBaseUrl+'/savePost',data,this.noAuthHeader);
  }
  
  getAllPosts(){    
     return this.http.get(environment.apiBaseUrl+'/allPosts',);
  }
  getPostsByUser(id){
    return this.http.get(environment.apiBaseUrl+'/getPostsByUser?_id='+id);

  }


}






