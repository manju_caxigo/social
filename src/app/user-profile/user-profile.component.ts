

import { Component, ViewChild, ElementRef,OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { UserService } from '../shared/user.service';
import { Router, ActivatedRoute } from "@angular/router";
import { PostService } from '../shared/post.service';

import { environment } from '../../environments/environment';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  userDetails;
  _id;
  nopost = false;
  posts: any[];
  user: any;
  request = false;
  friend = false;
  friendrequest = false;
  imageUrl='';
  @ViewChild('UploadFileInput', { static: false }) uploadFileInput: ElementRef;
  fileUploadForm: FormGroup;
  fileInputLabel: string;
  constructor(private userService: UserService, private router: Router,
    private postService: PostService,
    private _activatedRoute: ActivatedRoute,
    private http: HttpClient,
    private formBuilder: FormBuilder,) { }

  ngOnInit() {
    this.fileUploadForm = this.formBuilder.group({
      uploadedImage: [''],
    });
    this._activatedRoute.params.subscribe(params => {
      console.log(params);
      if (!params.id) {
        this.userService.getUserProfile().subscribe(
          res => {
            this.userDetails = res['user'];
            console.log("userDetails",this.userDetails.image)
            this.getMyPosts();
          },
          err => {
            console.log(err);

          }
        );
      } else {
        this._id = params.id
        this.getSingleUserDetails(this._id);

      }
    })



  }
  getSingleUserDetails(id) {
    this.userService.getUserById(id).subscribe(
      res => {
        this.userDetails = res['user'];
        console.log("details", this.userDetails)
        this.getMyPosts();
        this.getUser();
      },
      err => {
        console.log(err);
      }
    );
  }
  getMyPosts() {
    this.postService.getPostsByUser(this.userDetails._id).subscribe(
      (res) => {
        console.log("posts", res);
        this.posts = res['post']
        if (!this.posts.length) this.nopost = true
      },
      err => {
        console.log(err);
      }
    );

  }
  getUser() {
    console.log("enter")
    this.userService.getUserProfile().subscribe(
      res => {
        this.user = res['user'];
        console.log("user---->", this.user, this.userDetails)
        let request = this.userDetails.request.filter(arr => {
          return arr.userId === this.user._id;
        });
        let friendrequest = this.user.request.filter(arr => {
          return arr.userId === this.userDetails._id;
        });

        let friend = this.userDetails.friendsList.filter(arr => {

          return arr.friendId === this.user._id;
        });
        if (friend.length) this.friend = true;
        if (request.length) this.request = true
        if (friendrequest.length) this.friendrequest = true

        console.log("request", this.request, this.friend);


      },
      err => {
        console.log(err);

      })

  }
  approve(event) {
    console.log("event", event)
    this.userService.approveRequest({
      _id: this.user._id,
      name: this.user.name,
      user_id: event._id,
      user_name: event.name

    }).subscribe(
      res => {
        console.log("response", res)
        this.getSingleUserDetails(this._id)
        alert("Now you and " + event.name + " are friends")
      },
      err => {
        console.log(err);

      })
  }
  reject(event) {
    console.log("event", this.user)
    this.userService.rejectRequest({
      _id: this.user._id,
      name: this.user.name,
      user_id: event._id,
      user_name: event.name

    }).subscribe(
      res => {
        alert("Friend request rejected!")
        this.getSingleUserDetails(this._id)
      },
      err => {
        console.log(err);

      })
  }

  sendRequest() {

    this.userService.sendRequest({
      _id: this.user._id,
      user_id: this.userDetails._id,
      name: this.user.name
    }).subscribe(result => {
      console.log("result", result)
      alert("Request is sent successfully!")
      this.getSingleUserDetails(this._id);
    },
      err => {
        console.log(err);

      })


  }

  onLogout() {
    this.userService.deleteToken();
    this.router.navigate(['/login']);
  }
  updatephoto(){}
  onFileSelect(event) {
    if (event.target.files && event.target.files[0]) {
        const file = event.target.files[0];
        this.fileInputLabel = file.name;
        this.fileUploadForm.get('uploadedImage').setValue(file)
        const reader = new FileReader();
        reader.onload = e => this.imageUrl = reader.result as string;
        reader.readAsDataURL(file);
        this.onFormSubmit();
    }
}
  onFormSubmit() {

    if (!this.fileUploadForm.get('uploadedImage').value) {
      alert('Select a photo!');
      return false;
    }
  

    const formData = new FormData();
    formData.append('uploadedImage', this.fileUploadForm.get('uploadedImage').value);
    formData.append('agentId', '007');

    console.log("formdata",formData)
    this.http
      .post<any>(environment.apiBaseUrl+'/uploadfile', formData).subscribe(response => {
        console.log("imageupp",response);
        if (response.statusCode === 200) {
          this.imageUrl =  response.uploadedFile.filename;
          this.userService.changeProfile({
            imageUrl:this.imageUrl,
            _id:this.userDetails._id

          }).subscribe(result=>{
            console.log("changepic",result)
          })
        }
      }, er => {
        console.log(er);
        alert(er.error.error);
      });
  }

}
